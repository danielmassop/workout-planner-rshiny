library(rvest) 

page <- read_html("https://strengthlevel.com/strength-standards") 
links = page %>% html_nodes("a") %>% html_attr('href') #Find the all links on the above homepage

View(links)

links2 = c() #Create an empty list

for (i in links){ 
  if (startsWith(i, "/strength-standards")){         #Add links that have the correct beginning
    links2 = c(links2, i)
  }
}

View(links2)

links2 = links2[-1] #Remove header and footer links that we dont want
links2 = links2[-c(100:103)]
links3 = links2

master = c() #Create empty dataframe to put each page in

for (i in links3){
  
  link = paste("https://strengthlevel.com", i, sep="")
  page = read_html(link) #get the complete link and read the new page
  
  tbls_ls <- page %>%                  ##extract the tables from the page (inspect some to see that there are 2 on each page)
    html_nodes("table") %>%
    .[1:2] %>%
    html_table(fill = TRUE)
  
  male = tbls_ls[1][[1]]          #The first one is the male, second one is female
  female = tbls_ls[2][[1]]
  
  male = apply(male,2,function(x)sub(" .*", "", x)) #Remove everything after the desired data cell for each column
  male = as.data.frame(male)
  male$gender = "male"            #Put table into a dataframe and add gender and exercise name
  male$exercise = basename(i)
  
  female = apply(female,2,function(x)sub(" .*", "", x))
  female = as.data.frame(female)    #Do same thing for female
  female$gender = "female"
  female$exercise = basename(i)
  
  master = rbind(master, male, female) #Add to master dataframe
}

for (i in 1:6){      #Convert from factor to a string
  master[,colnames(master)[i]] = as.numeric(as.character(master[,colnames(master)[i]]))
}

master[is.na(master)] <- 0 #Replace all missing numbers with zero

write.csv(master, "exercise_benchmarks.csv") #write dataframe to a csv file on your computer
